import requests

from config import *


def send_message(chat_id, message):
    api_url = f'https://api.telegram.org/bot{TOKEN}/sendMessage'

    try:
        print(f"Try send message to [{chat_id}]: [{message}]")
        response = requests.post(
            api_url,
            json={
                "chat_id": chat_id,
                "text": message
            }
        )
        print(f"Response: {response}, {response.json()}")
    except Exception as exc:
        print(f"Request Error: {exc}")


if __name__ == "__main__":
    for key, value in TEST_IDS.items():
        send_message(value, f"TEST_ID = {key}")


# telebot_module -> get id by user name
# send via used id - any case
