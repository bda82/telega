import requests
from config import *


url = f"https://api.telegram.org/bot{TOKEN}/"


def get_updates_json(request):
    response = requests.get(request + 'getUpdates')
    return response.json()


def last_update(data):
    telega = data['result']
    total_updates = len(telega) - 1
    results = telega[total_updates]['message']['from']
    print(f"id :{results['id']}, Имя: {results['first_name']}")


last_update(get_updates_json(url))
