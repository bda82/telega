import requests

from config import *


def telegram_bot_sendtext(bot_message):

    send_text = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?chat_id=' + MY_ID_3 + '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

    return response.json()


test = telegram_bot_sendtext("Testing Telegram bot")
print(test)
