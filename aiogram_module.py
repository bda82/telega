import asyncio
from aiogram import Bot

from config import *


async def send_message(chat_id, message):
    operator = Bot(TOKEN)
    try:
        print(f"Try send message to [{chat_id}]: [{message}]")
        response = await operator.send_message(chat_id, message)
        print(f"Response: {response}")
    except Exception as exc:
        print(f"Request Error: {exc}")


def main():
    for key, value in TEST_IDS.items():
        asyncio.run(send_message(value, f"TEST_ID = {key}"))


if __name__ == "__main__":
    main()
