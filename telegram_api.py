import requests

from config import *


def send_message(user_id, message):
    api_url = f'https://api.telegram.org/bot{TOKEN}/sendMessage'

    try:
        print(f"Try send message to [{user_id}]: [{message}]")
        response = requests.post(
            api_url,
            json={
                "chat_id": user_id,
                "text": message
            }
        )
        print(f"Response: {response}, {response.json()}")
    except Exception as exc:
        print(f"Request Error: {exc}")


if __name__ == "__main__":
    send_message(234031620, "TEST MESSAGEID!")
