import asyncio
import telegram

from config import *


async def send_message(chat_id, message):
    bot = telegram.Bot(token=TOKEN)
    try:
        print(f"Try send message to [{chat_id}]: [{message}]")
        response = await bot.send_message(chat_id=chat_id, text=message)
        print(f"Response: {response}")
    except Exception as exc:
        print(f"Request Error: {exc}")


def main():
    for key, value in TEST_IDS.items():
        asyncio.run(send_message(value, f"TEST_ID = {key}"))


if __name__ == "__main__":
    main()
