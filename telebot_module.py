import telebot
from config import TOKEN

bot = telebot.TeleBot(TOKEN)


@bot.message_handler(commands=['start'])
def start(message):
    print('user_id', message.from_user.id, 'username', message.from_user.username)
    bot.reply_to(message, "Добро пожаловать в Отелло! Ваши данные учтены.")
    # Теперь можно сохранить в БД user ID и username
    # поставив их в соответствие username из таблиц личной информации Отельера


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    print('user_id', message.from_user.id, 'username', message.from_user.username)
    # bot.reply_to(message, "Вы что-то поменяли?")
    send_raw_message(message.from_user.id, "Привет юфззеер!!! Ватсааап!!!")


@bot.message_handler(content_types=['text'])
def send_raw_message(user_id, message):
    bot.send_message(user_id, message)


bot.polling(none_stop=True, interval=0)
